package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AdidasHomePage extends Page {

	public AdidasHomePage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private final String samplePageLocator = ".//li[@id='menu-item-54']/a";

	
	
	@FindBy(how = How.XPATH, using = samplePageLocator)
	private WebElement samplePageLocatortElement;
	

	public void goToSamplePage() {

		samplePageLocatortElement.click();

	}


	
}
