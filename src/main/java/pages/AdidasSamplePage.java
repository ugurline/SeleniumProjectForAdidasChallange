package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AdidasSamplePage extends Page {

	private final String textAreaLocator = ".//p/textarea";
	private final String nameLocator = ".//input[@id='author']";
	private final String emailLocator = ".//input[@id='email']";
	private final String submitButtonLocator = ".//input[@name='submit']";
	private final String authorNameLocator= ".//div[@class='comment-author vcard']/cite";
	private final String commentLocator= ".//div[@class='comment_wrap group']/div[2]/p";
	
	@FindBy(how = How.XPATH, using = textAreaLocator)
	private WebElement textAreaLocatorElement;
	@FindBy(how = How.XPATH, using = nameLocator)
	private WebElement nameLocatorElement;
	@FindBy(how = How.XPATH, using = emailLocator)
	private WebElement emailLocatorElement;
	@FindBy(how = How.XPATH, using = submitButtonLocator)
	private WebElement submitButtonLocatorElement;
	@FindBy(how = How.XPATH, using = authorNameLocator)
	private WebElement authorNameLocatorElement;
	@FindBy(how = How.XPATH, using = commentLocator)
	private WebElement commentLocatorElement;
	
	
	public AdidasSamplePage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	private List<String> getCommentAuthorNames() {
		waitForWebElement(authorNameLocatorElement);
		List<WebElement> listOfAuthorNamesWebelements= driver.findElements(By.xpath(authorNameLocator));
		List<String> listOfAuthorNames = new ArrayList<String>();
		for(int i=0;i<listOfAuthorNamesWebelements.size();i++) {
			listOfAuthorNames.add(listOfAuthorNamesWebelements.get(i).getText());
		}
		System.out.println("listOfAuthorNames : " + listOfAuthorNames);

		return listOfAuthorNames;
	}
	
	private List<String> getComments() {
		waitForWebElement(commentLocatorElement);
		List<WebElement> listOfCommentsWebElements= driver.findElements(By.xpath(commentLocator));
		List<String> listOfComments = new ArrayList<String>();

		for(int i=0;i<listOfCommentsWebElements.size();i++) {
			listOfComments.add(listOfCommentsWebElements.get(i).getText());
		}
		System.out.println("listOfComments : " + listOfComments);
		return listOfComments;


	}
	
	public boolean isNamePostedSuccessfully(String actualName) {
		
		boolean status = false;
		List<String> listOfAuthorNames = getCommentAuthorNames();
		if(listOfAuthorNames.contains(actualName)) {
			status = true;
		}
		return status;
	}
	
	public boolean isCommentPostedSuccessfully(String actualComment) {
		
		boolean status = false;
		List<String> listOfComments = getComments();
		if(listOfComments.contains(actualComment)) {
			status = true;
		}
		return status;
	}
	
	public void inputTextForTextArea(String text) {
		waitForWebElement(textAreaLocatorElement, 8);
		textAreaLocatorElement.clear();
		textAreaLocatorElement.sendKeys(text);
	}
	
	public void inputName(String name) {
		nameLocatorElement.clear();
		nameLocatorElement.sendKeys(name);
	}
	
	public void inputEmail(String email) {
		emailLocatorElement.clear();
		emailLocatorElement.sendKeys(email);
	}
	
	public void clickSubmitButton() {
		
		submitButtonLocatorElement.click();
	}

}
