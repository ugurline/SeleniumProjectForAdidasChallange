package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Page {

	WebDriver driver;

	public Page(WebDriver driver) {
		this.driver = driver;
	}
	
	public void waitForWebElement(WebElement webElement) {
		waitForWebElement(webElement,10);
		}
	
	public void waitForWebElement(WebElement webElement , long waitTime) {
		WebDriverWait wait = new WebDriverWait(driver, waitTime);
		wait.until(ExpectedConditions.visibilityOf(webElement));
		
	}

}