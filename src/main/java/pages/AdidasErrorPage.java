package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AdidasErrorPage extends Page {

	
	private final String errorMessageLocator = ".//body[@id='error-page']/p[2]";
	private final String backButton = ".//p/a";
	
	@FindBy(how = How.XPATH, using = errorMessageLocator)
	private WebElement errorMessageLocatortElement;
	@FindBy(how = How.XPATH, using = backButton)
	private WebElement backButtonElement;
	
	
	public AdidasErrorPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	private String getErrorMessage() {
		
		return errorMessageLocatortElement.getText();
	}
	
	public boolean isErrorMessageDisplayed() {
		
		boolean status = false;
		
		if(getErrorMessage().equals("ERROR: please enter a valid email address.")) {
			status = true;
		}
		
		return status;
	}
	
	public void clickBackButton() {
		
		backButtonElement.click();
	}

}
