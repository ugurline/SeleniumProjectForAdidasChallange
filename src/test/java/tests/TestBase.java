package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class TestBase {

	WebDriver driver;

	String siteUrl = "http://store.demoqa.com/";

	@BeforeClass
	public void initialize() {
		
		if(System.getProperty("os.name").contains("Windows")) {
			System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
		}
		else {
			System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver");
		}
		driver = new FirefoxDriver();		
		driver.get(siteUrl);
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}
