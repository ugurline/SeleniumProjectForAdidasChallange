package tests;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pages.AdidasErrorPage;
import pages.AdidasHomePage;
import pages.AdidasSamplePage;
import util.Helpers;

public class TestCommentFunction extends TestBase {
	AdidasHomePage adidasHomePage;
	AdidasSamplePage adidasSamplePage;
	AdidasErrorPage adidasErrorPage;
	private String actualComment;
	private String actualName;
	private String invalidMail;
	private String validMail;

	@BeforeClass
	private void initPage() {

		adidasHomePage = PageFactory.initElements(driver, AdidasHomePage.class);
		adidasSamplePage = PageFactory.initElements(driver, AdidasSamplePage.class);
		adidasErrorPage = PageFactory.initElements(driver, AdidasErrorPage.class);
		adidasHomePage.goToSamplePage();

	}

	@BeforeMethod
	private void buildInput() {

		actualComment = Helpers.generateRandomInput("Comment");
		actualName = Helpers.generateRandomInput("Ugur");
		invalidMail = "Invalid Mail";
		validMail = "correct_email@gmail.com";
	}

	@Test(priority = 1)
	private void testWithWrongEmail() {

		adidasSamplePage.inputTextForTextArea(actualComment);
		adidasSamplePage.inputName(actualName);
		adidasSamplePage.inputEmail(invalidMail);
		adidasSamplePage.clickSubmitButton();
		assertTrue(adidasErrorPage.isErrorMessageDisplayed(), "error message is not displayed properly!");
		adidasErrorPage.clickBackButton();
	}

	@Test(priority = 2)
	private void testWithCorrectEmail() {

		System.out.println("actualName : " + actualName);
		System.out.println("actualComment : " + actualComment);

		adidasSamplePage.inputTextForTextArea(actualComment);
		adidasSamplePage.inputName(actualName);
		adidasSamplePage.inputEmail(validMail);
		adidasSamplePage.clickSubmitButton();
		assertTrue(adidasSamplePage.isNamePostedSuccessfully(actualName), "name couldn't be found in page");
		assertTrue(adidasSamplePage.isCommentPostedSuccessfully(actualComment), "comment couldn't be found in page");

	}

}
