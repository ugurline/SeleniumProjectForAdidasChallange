## Synopsis

Purpose of this project is to run automated tests for http://store.demoqa.com/ for given scenarios in the challange.

## Code Example

I believe that best documentation for code is the code itself. So I always try to make it clear and understandable by third parties.
I rarely use comments because I make namings clear.

I have three packages in the project: pages, utils and tests.

Main idea is to have a page class for each page I navigate to achieve the goals.  
Each page class keeps webelements present in that page. I implmeneted only web-elements that I need in the scenario. I also have methods that interact with the webelements.
All page classes extends Page class which contains driver.

Util package keeps helper class which would be used to have helping methods, such as random number generation.

Test package keeps test classes which contains the test cases for different scenarios. To deliver the sceario in the description
I decided to divide the scenario into two : testWithWrongEmail and testWithWrongEmail. These will run in an order because of the request ("go back" action)
At the end of each test method I'm asserting necessary things to make sure they're tested correctly. I try to avoid implementing "logic operations" in test cases and keep those in
relevant Page object to keep test cases clean, black boxed, and understandable.


## Motivation
Written upon request by ADIDAS

## Installation
This is a maven project, written on Eclipse Version: Oxygen.3a Release (4.7.3a).

Test framework I'm using is TestNG.

Browser driver I'm using is geckodriver for mozilla. In the project I included Windows and Linux versions, so tests should run in both environments with no problem.
But I didn't have time to test it on Linux. I wanted to implement a driver factory class to use different browsers upon request, but I had no time for that.

**To run the tests you will need maven installed on the machine.**

## Tests
To run the tests, first pull the project to local and while in project folder, go to command line use **"mvn install"** command to get all dependencies.

Then use **"mvn -Dtest=TestCommentFunction test"** to run the test class.

Tester can also run the tests in Eclipse by right clicking TestCommentFunction class -> Run As -> TestNG test

## Contributors
Ugur Aydin
